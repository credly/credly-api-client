<?php

return [
	'baseUrl' => CREDLY_BASE_URL,
	'operations' => [
		'authenticate' => [
			'httpMethod' => 'POST',
			'uri' => CREDLY_API_VERSION . '/authenticate',
			'description' => 'Authenticate with the API and retrieve tokens',
			'responseModel' => 'AuthenticateResponse',
			'parameters' => [
				'Authorization' => [
					'type' => 'string',
					'location' => 'header',
				],
			],
		],
		'authenticate/register' => [
			'httpMethod' => 'POST',
			'uri' => CREDLY_API_VERSION . '/authenticate/register',
			'description' => 'Register a new Credly member',
			'responseModel' => 'AuthenticateResponse',
			'parameters' => [
				'email' => [
					'type' => 'string',
					'location' => 'postField',
				],
				'password' => [
					'type' => 'string',
					'location' => 'postField',
				],
				'first_name' => [
					'type' => 'string',
					'location' => 'postField',
				],
				'last_name' => [
					'type' => 'string',
					'location' => 'postField',
				],
				'display_name' => [
					'type'     => 'string',
					'location' => 'postField',
					'default'  => '',
				],
				'is_organization' => [
					'type'     => 'string',
					'location' => 'postField',
					'default'  => '0',
				],
				'is_email_verified' => [
					'type'     => 'string',
					'location' => 'postField',
					'default'  => '1',
				],
				'is_pro' => [
					'type'     => 'string',
					'location' => 'postField',
					'default'  => '0',
				],
			],
		],
		'me' => [
			'httpMethod' => 'GET',
			'uri' => CREDLY_API_VERSION . '/me',
			'description' => 'Fetch the current authenticated member',
			'responseModel' => 'MeResponse',
			'parameters' => [],
		],
	],
	'models' => [
		'AuthenticateResponse' => [
			'type' => 'object',
			'location' => 'json',
			'properties' => [
				'meta' => [
					'type' => 'object',
					'required' => true,
				],
				'data' => [
					'type' => 'object',
					'required' => true,
				],
			],
		],
		'MeResponse' => [
			'type' => 'object',
			'location' => 'json',
			'properties' => [
				'meta' => [
					'type' => 'object',
					'required' => true,
				],
				'data' => [
					'type' => 'object',
					'required' => true,
				],
			],
		],
	],
];
