<?php

namespace Credly\Api\Exception;

use GuzzleHttp\Command\Exception\CommandException;

class ApiException extends \Exception {

	public function __construct($message, $code = 0, CommandException $previous = null) {
		parent::__construct($message, $code, $previous);
	}

	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}

}
