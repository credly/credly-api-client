# Credly API Client

> **Note:** This SDK is a work in progress and does not include all available Open Credit API endpoints.

## Install

Add the following to your composer.json:

```json
{
	"repositories": [
		{
			"url": "git@bitbucket.org:credly/credly-api-client.git",
			"type": "vcs"
		}
	],
	"require": {
		"credly/api-client": "dev-master"
	}
}
```
## Usage

```php
<?php
	$client = new \Credly\Api\Client('YOUR_API_KEY', 'YOUR_API_SECRET');
	$tokens = $client->register('kdomingo@credly.com', 'Kathy', 'Domingo', 'paSSw0rD');
```
## Testing

In order to run the unit tests, you'll need to copy the `.env.example` file to `.env` in the root directory of this project. Inside you'll need a valid Credly API key and secret, along with a valid email address and password that belongs to an existing account. See https://developers.credly.com for details on how to obtain app credentials.

Once those are in place, you can run the tests via `./vendor/bin/phpunit`.
