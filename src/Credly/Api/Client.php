<?php
/**
* Client is a Credly API interface.
*
* This class provides a simple wrapper around a GuzzleClient
* in order to perform Credly API calls from PHP.
*
* Example usage:
* $client = new \Credly\Api\Client('foo', 'bar');
* $client->register('kdomingo@credly.com', 'Kathy', 'Domingo', 'paSSw0rD');
*
* @package  credly/api-client
* @author   Credly Developers <developers@credly.com>
* @see      https://developers.credly.com
*/

namespace Credly\Api;

use Credly\Api\Exception\ApiException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Command\Exception\CommandException;
use Guzzle\Service\Loader\PhpLoader;
use Symfony\Component\Config\FileLocator;

class Client {

	/**
	 * The instance of GuzzleClient.
	 * @var GuzzleClient
	 */
	private $_client = null;

	/**
	 * The Credly API key.
	 * @var string
	 */
	protected $key = null;

	/**
	 * The Credly API secret.
	 * @var string
	 */
	protected $secret = null;

	/**
	 * The Credly API access token.
	 * @var string
	 */
	protected $access_token = null;

	/**
	 * The constructor.
	 * @param string $key
	 * @param string $secret
	 * @param string $access_token
	 * @param string $base_url
	 * @param string $version
	 */
	public function __construct($key = null, $secret = null, $access_token = null, $base_url = 'https://api.credly.com', $version = 'v1.1')
	{
		// Sock away somewhat constant identifiers.
		$this->key = $key;
		$this->secret = $secret;
		$this->access_token = $access_token;

		// Store a few constants.
		! defined('CREDLY_BASE_URL') && define('CREDLY_BASE_URL', $base_url);
		! defined('CREDLY_API_VERSION') && define('CREDLY_API_VERSION', '/' . $version);

		// Instantiate a new GuzzleClient, while fetching the description object.
		$this->_client = new GuzzleClient(
			new HttpClient,
			new Description($this->buildDescription($key, $secret))
		);
	}

	/**
	 * Access token setter.
	 * @param string $token
	 */
	public function setAccessToken($token)
	{
		$this->access_token = $token;
	}

	/**
	 * Builds the configuration for the Guzzle Description.
	 * @param  string $key
	 * @param  string $secret
	 * @return array
	 */
	private function buildDescription($key, $secret)
	{
		$locator     = new FileLocator([__DIR__ . '/Description']);
		$loader      = new PhpLoader($locator);
		$description = $loader->load($locator->locate('description.php'));

		// Walk through each operation and apply our default params.
		foreach ($description['operations'] as $name => $op) {
			$description['operations'][$name]['parameters']['X-Api-Key'] = [
				'type' => 'string',
				'location' => 'header',
				'default' => $key,
			];
			$description['operations'][$name]['parameters']['X-Api-Secret'] = [
				'type' => 'string',
				'location' => 'header',
				'default' => $secret,
			];
			$description['operations'][$name]['parameters']['access_token'] = [
				'type' => 'string',
				'location' => 'query',
				'required' => false,
			];
		}

		return $description;
	}

	/**
	 * Generic handler for invoking Guzzle commands.
	 * @param  string  $endpoint
	 * @param  array   $params
	 * @param  boolean $authenticated
	 * @throws ApiException if the API response is non-2XX.
	 * @return array
	 */
	private function invokeApi($endpoint, $params = [], $authenticated = true)
	{
		try {
			// If this endpoint should be authenticated, then shim in the token.
			if ($authenticated) {
				$params['access_token'] = $this->access_token;
			}

			return $this->_client->{$endpoint}($params);
		} catch (CommandException $e) {
			$response = $e->getResponse()->json();

			// Compile a meaningful error message.
			$message = $response['meta']['status'] . ' : ' . $response['meta']['message'];

			// Include any additional information.
			if (isset($response['meta']['more_info']) && ! empty($response['meta']['more_info'])) {
				$message .= ' (' . json_encode($response['meta']['more_info']) . ')';
			}

			// Throw our own exception.
			throw new ApiException($message, $e->getCode(), $e);
		}
	}

	/**
	 * Authenticate a Credly member. Invokes POST /authenticate.
	 * @param  string $email
	 * @param  strign $password
	 * @return array
	 */
	public function authenticate($email, $password)
	{
		return $this->invokeApi('authenticate', [
			'Authorization' => 'Basic ' . base64_encode($email . ':' . $password),
		], false);
	}

	/**
	 * Register a Credly member. Invokes POST /authenticate/register.
	 * @param  string $email
	 * @param  string $first_name
	 * @param  string $last_name
	 * @param  string $password
	 * @return array
	 */
	public function register($email, $first_name, $last_name, $password)
	{
		return $this->invokeApi('authenticate/register', [
			'email' => $email,
			'password' => $password,
			'first_name' => $first_name,
			'last_name' => $last_name,
		], false);
	}

	/**
	 * Fetch the current authenticated Credly member. Invokes GET /me.
	 * @return array
	 */
	public function me()
	{
		return $this->invokeApi('me');
	}
}
