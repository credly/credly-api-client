<?php

use Credly\Api\Client;

class ClientTest extends PHPUnit_Framework_TestCase {

	private $client = null;

	/**
	 * Set up our API client for testing.
	 * @return void
	 */
	public function setUp()
	{
		$env = new \Dotenv\Dotenv(__DIR__ . '/../');
		$env->load();

		$this->client = new Client(
			getenv('X_API_KEY'),
			getenv('X_API_SECRET'),
			null,
			getenv('API_BASE_URL')
		);
	}

	/**
	 * Testing ApiExceptions.
	 * @return void
	 */
	public function testApiException()
	{
		$this->setExpectedException('Credly\Api\Exception\ApiException');
		$this->client->authenticate('foo', 'bar');
	}

	/**
	 * Testing authentication.
	 * @return void
	 */
	public function testAuthenticate()
	{
		$result = $this->client->authenticate(getenv('EMAIL'), getenv('PASSWORD'));

		$this->assertArrayHasKey('token', $result['data']);
		$this->assertArrayHasKey('refresh_token', $result['data']);
	}

	/**
	 * Testing registration.
	 * @return array
	 */
	public function testRegister()
	{
		$result = $this->client->register(
			'success+c' . time() . '@simulator.amazonses.com',
			'Credly Test ' . time(),
			'Last',
			uniqid()
		);

		$this->assertArrayHasKey('token', $result['data']);
		$this->assertArrayHasKey('refresh_token', $result['data']);

		return $result['data'];
	}

	/**
	 * Testing fetching the current authenticated member.
	 * @return  void
	 * @depends testRegister
	 */
	public function testMe($tokens)
	{
		$this->client->setAccessToken($tokens['token']);

		$result = $this->client->me();

		$this->assertArrayHasKey('id', $result['data']);
	}
}
